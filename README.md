# Setoko

[![pipeline status](https://gitlab.com/AlifSaddid/setoko/badges/master/pipeline.svg)](https://gitlab.com/AlifSaddid/setoko/-/commits/master)
[![coverage report](https://gitlab.com/AlifSaddid/setoko/badges/master/coverage.svg)](https://gitlab.com/AlifSaddid/setoko/-/commits/master)




## Pengembang Setoko (A09)
1. 1906353656 Syndi Yohana Sitorus
2. 1906353851 Faatihah Tharra Sabbih
3. 1906353984 Harakan Akbar
4. 1906398212 Ageng Anugrah Wardoyo Putra
5. 1906398250 Muhammad Alif Saddid

## Tautan
**Setoko** dapat diakses di [Setoko](https://setoko.herokuapp.com).

## Apa itu Setoko?
Di masa pandemi Covid-19, banyak sekali kegiatan ekonomi yang terpaksa berhenti karena pemberlakuan pembatasan sosial. Orang-orang pun ketakutan untuk pergi ke luar rumah karena khawatir tertular. **Setoko** memfasilitasi pelaku Usaha Mikro Kecil Menengah (UMKM) agar tetap bisa menjalankan bisnisnya selama masa pandemi.

Ok so it is basically an e-commerce app.

## Fitur Setoko

**1. Jual beli barang (/toko, /checkout, and /details)**

   Setiap UMKM bisa menjual barangnya di Setoko (It's an e-commerce, right?).


**2. Contact Us (It's in the footer)**

   Pengguna Setoko bisa memberikan pesan ke administrator Setoko apabila diperlukan.
   

**3. Donasi (/donasi)**

   Setiap orang bisa mendonasikan uang ke orang lain (“No-one has become poor by giving.”)
