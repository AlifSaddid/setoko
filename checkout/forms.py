from django import forms
from .models import dataCekot


class formCekot(forms.Form):

    class Meta:
        model = dataCekot
        fields = ('__all__')

    attrs = {'class': 'form-control'}
    namaDepan = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Nama Depan', max_length=100, required=True)
    namaBelakang = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Nama Belakang', max_length=100, required=True)
    email = forms.CharField(widget=forms.Textarea(
        attrs=attrs), label='Email ',max_length=100, required=True)
    Alamat = forms.CharField(widget=forms.TextInput(
        attrs=attrs), label='Alamat', max_length=3000, required=True)
