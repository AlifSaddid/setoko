from django.db import models
from toko.models import Barang

class dataCekot(models.Model):

    namaDepan = models.CharField(max_length=100)
    namaBelakang = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    alamat = models.CharField(max_length=3000)
    idpenjual = models.CharField(max_length=3000)
    idpembeli = models.CharField(max_length=3000)
    item = models.ForeignKey(Barang, on_delete = models.CASCADE)
    jumlah = models.IntegerField()

