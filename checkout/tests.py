from django.test import TestCase, Client
from django.urls import resolve
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile
from io import BytesIO, StringIO

from django.contrib.auth.models import User 
from toko.models import Barang, Notifikasi, Cart
from .views import checkout, cekot
from .models import dataCekot

# Create your tests here.
class checkoutTest(TestCase):

     # URL TestCases
    def test_url_is_exist_checkout(self):
        response = Client().get('/checkout/')
        self.assertEqual(response.status_code, 302)

    def test_url_is_exist_checkout_riwayat(self):
        response = Client().get('/checkout/riwayat/')
        self.assertEqual(response.status_code, 200)

    # Model TestCases
    def test_model_dataCekot_create(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )

        barang_baru.save()
        new_pembeli = User.objects.create_user('pembeli', '', 'password_for_pembeli')
        new_pembeli.set_password('password_for_pembeli')
        new_pembeli.save()

        dataCekot_baru = dataCekot.objects.create(
            namaDepan = 'haha',
            namaBelakang = 'hihi',
            email = 'contoh@gmail.com',
            alamat = 'jakarta',
            idpenjual = new_account.id,
            idpembeli =  new_pembeli.id,
            item = barang_baru,
            jumlah = 3
        )
        count_all_dataCekot = dataCekot.objects.all().count()
        self.assertEqual(count_all_dataCekot, 1)

    # Views TestCases
    def test_view_checkout_cannot_accessed_from_get_request(self):
        response = Client().get("/checkout/")
        self.assertEqual(response.status_code,302)

    def test_view_save_cekot_from_post_req(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()

        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')

        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'Barang',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )
        
        new_pembeli = User.objects.create_user('pembeli', '', 'password_for_pembeli')
        new_pembeli.set_password('password_for_pembeli')
        new_pembeli.save()

        c = Client()
        login = c.login(username=new_pembeli.username, password='password_for_pembeli')
        
        barang = Barang.objects.get(id=1)

        cart = Cart.objects.create(penjual = new_account, id_barang = barang.id, jumlah_barang = 1)
        
        response = c.post(
            "/checkout/checkout/",
            data = {
                'items': [cart.id],
                'nama_depan': "apalah",
                'nama_belakang' : "iyagitu",
                'input_email_user' : 'haha@gmail.com',
                'alamat' : "blabla",
                'id_penjual' : new_account.id,
                'jumlah' : 3 
            }
        )
        count_all_dataCekot = dataCekot.objects.all().count()
        self.assertEqual(count_all_dataCekot, 1)