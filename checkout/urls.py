from django.urls import path
from . import views

app_name = 'checkout'

urlpatterns = [
    path('', views.checkout, name = 'checkout'),
    path('checkout/', views.cekot, name = 'cekot'),
    path('riwayat/', views.riwayat, name = 'riwayat'),
]
