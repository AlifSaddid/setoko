from django.shortcuts import render,redirect
from django.http import QueryDict
from .models import dataCekot
from toko.models import Barang, Notifikasi, Cart
from .forms import formCekot
from django.contrib.auth.models import User 
from django.contrib.auth import get_user_model

# Create your views here.
def checkout(request):
    if (request.user.is_anonymous):
        return redirect('toko:index')
    if request.method == 'POST':
        data = dict(request.POST.lists())
        items = [(Barang.objects.get(id = cart.id_barang), cart.jumlah_barang, cart.id) for cart in [Cart.objects.get(id = cart_id) for cart_id in data['carts']]]
        total = 0
        for barang, jumlah, id in items:
            total += (int(barang.harga * int(jumlah)))
        context = {
            'items': items,
            'total': total,
        }
        return render(request, 'checkout/checkout.html', context)
    else:
        return redirect('toko:index')

def cekot(request):
    if request.method == 'POST':
        data = dict(request.POST.lists())
        user = get_user_model()
        
        for cart_id in data['items']:
            cart = Cart.objects.get(id = cart_id)
            barang = Barang.objects.get(id = cart.id_barang)
            nama_depan = request.POST['nama_depan']
            nama_belakang = request.POST['nama_belakang']
            email = request.POST['input_email_user']
            alamat = request.POST['alamat']
            idbarang = barang.id
            idpenjual = barang.penjual.id
            jumlah = cart.jumlah_barang

            data_baru = dataCekot.objects.create(
                namaDepan = nama_depan,
                namaBelakang = nama_belakang,
                email = email,
                alamat = alamat,
                idpenjual = idpenjual,
                idpembeli = request.user.id,
                item = barang,
                jumlah = jumlah
            )
            data_baru.save()

            
            User = get_user_model()
            penjual = User.objects.get(id = idpenjual)
            pembeli = User.objects.get(id = request.user.id)
            
            isi_notifikasi = pembeli.username + " membeli barang anda " + barang.nama + " sejumlah " + str(jumlah)
            notif_baru = Notifikasi.objects.create(penjual = penjual, isi = isi_notifikasi)
            notif_baru.save()
        
        for cart_id in data['items']:
            cart = Cart.objects.get(id = cart_id)
            cart.delete()
        
    return redirect('toko:index')


def riwayat(request):
    semuaData = dataCekot.objects.filter(idpembeli=request.user.id)
    context = {
        "allData":semuaData
    }
    return render(request, 'checkout/riwayat.html', context)
    
