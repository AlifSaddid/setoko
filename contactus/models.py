from django.db import models

# Create your models here.

class KesanPesan(models.Model):
    Nama=models.CharField(max_length=70)
    Email=models.EmailField(max_length=70)
    Pesan=models.TextField(max_length=1000,null=True)

    def __str__(self):
        return self.Nama
