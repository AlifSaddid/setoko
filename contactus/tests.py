from django.test import TestCase,Client
from django.urls import resolve, reverse
from django.apps import apps
from http import HTTPStatus

from .apps import ContactusConfig
from .models import KesanPesan
from .views import simpan,testimoni
import json
from django.contrib.auth.models import User

# Create your tests here.

class contactusTest(TestCase):
    def test_apps(self):
        self.assertEqual(ContactusConfig.name, 'contactus')
        self.assertEqual(apps.get_app_config('contactus').name, 'contactus')

    def test_models(self):
        object=KesanPesan.objects.create(
            Nama='Harakan Akbar',
            Email='harakanakbar2@gmail.com',
            Pesan='Semoga lulus ppw amin '
        )
        self.assertEqual(object.__str__(),'Harakan Akbar')
    
    def test_models_nama(self):
        object=KesanPesan.objects.create(
            Nama='Harakan Akbar',
            Email='harakanakbar2@gmail.com',
            Pesan='Semoga lulus ppw amin '
        )
        object.save()
        self.assertEqual(KesanPesan.objects.get(id=1).Nama,'Harakan Akbar')
    
    def test_models_email(self):
        object=KesanPesan.objects.create(
            Nama='Harakan Akbar',
            Email='harakanakbar2@gmail.com',
            Pesan='Semoga lulus ppw amin '
        )
        object.save()
        self.assertEqual(KesanPesan.objects.get(id=1).Email,'harakanakbar2@gmail.com')

    def test_models_pesan(self):
        object=KesanPesan.objects.create(
            Nama='Harakan Akbar',
            Email='harakanakbar2@gmail.com',
            Pesan='Semoga lulus ppw amin'
        )
        object.save()
        self.assertEqual(KesanPesan.objects.get(id=1).Pesan,'Semoga lulus ppw amin')

    def test_views_simpan_POST(self):
        c=Client()
        response=c.post(
            '/contactus/simpan/',
            data={
                'Nama' : 'Harakan Akbar',
                'Email' : 'harakanakbar2@gmail.com',
                'Pesan': 'Bagus Sekali',
            }
        )
        self.assertEqual(response.status_code, 200)

    def test_views_simpan_POST_can_simpan_if_not_login(self):
        c=Client()
        response=c.post(
            '/contactus/simpan/',
            data={
                'Nama' : 'Harakan Akbar',
                'Email' : 'harakanakbar2@gmail.com',
                'Pesan': 'Bagus Sekali',
            }
        )
        self.assertEqual(KesanPesan.objects.all().count(),1)


    def test_views_simpan_POST_can_simpan_when_login(self):
        new_account = User.objects.create_user('akbar', '', 'halo')
        new_account.set_password('halo')
        new_account.save()
        c= Client()
        login= c.login(username=new_account.username, password='halo')
        response=c.post(
            '/contactus/simpan/',
            data={
                'Email' : 'harakanakbar2@gmail.com',
                'Pesan': 'Bagus Sekali',
            }
        )
        self.assertEqual(KesanPesan.objects.all().count(),1)
        self.assertEqual(KesanPesan.objects.get(id=1).Nama, new_account.username )
    
    def test_views_testimoni_GET(self):
        c=Client()
        response=Client().get('/contactus/testimoni')
        self.assertEqual(response.status_code, 200)

    





