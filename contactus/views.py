from django.shortcuts import redirect, render
from django.contrib.auth.models import User 
from django.core import serializers
from django.http import HttpResponse
import json

from .models import KesanPesan

# Create your views here.

def simpan(request):
    response={}
    nama_input = ""
    if (request.method =='POST'):
        if (request.user.is_anonymous):
            nama_input=request.POST.get('input_nama',False)
            email_input=request.POST.get('input_email_user',False)
            pesan_input=request.POST.get('input_pesan',False)
            if ( pesan_input != ""):
                KesanPesan.objects.create(
                    Nama=nama_input,
                    Email=email_input,
                    Pesan=pesan_input
                )
        else:
            nama_input = request.user.username
            email_input=request.POST.get('input_email_user',False)
            pesan_input=request.POST.get('input_pesan',False)
            if ( pesan_input != ""):
                KesanPesan.objects.create(
                    Nama= request.user.username,
                    Email=email_input,
                    Pesan=pesan_input
                ) 
    response['message'] = nama_input
    return HttpResponse(json.dumps(response), content_type="application/json")

def testimoni(request):
    kesanpesan_json = serializers.serialize("json", KesanPesan.objects.all())
    return HttpResponse(json.dumps(kesanpesan_json), content_type="application/json")