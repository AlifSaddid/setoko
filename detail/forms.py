from django import forms
from .models import Ulasan

class UlasanForm(forms.ModelForm):
    class Meta:
        model      = Ulasan
        fields     = [
            'ulasan',
            'id_barang',
            'user',
        ]
        widgets = {
            'ulasan': forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan ulasan mengenai barang'
                }
            ),
        }