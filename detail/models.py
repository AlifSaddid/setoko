from django.db import models
from toko.models import Barang
from django.conf import settings

# Create your models here.
class Ulasan(models.Model):
    ulasan = models.CharField(max_length = 150)
    id_barang = models.IntegerField()
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, 
        on_delete = models.CASCADE
    )
    def __str__(self):
        return self.ulasan