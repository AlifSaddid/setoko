from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User 
from .apps import DetailConfig
from .models import Ulasan
from .views import detailbarang, simpanUlasan
from toko.models import Barang, Notifikasi 

# Create your tests here.
class DetailTest(TestCase):
    def test_apps(self):
        self.assertEqual(DetailConfig.name, 'detail')
    
    #URL
    def test_url_is_exist_detail_get_failed(self):
        response = Client().get('/detail/')
        self.assertEqual(response.status_code, 404)

    def test_url_is_exist_detail(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        barang = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = Client().get('/detail/' + str(barang.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_url_ada_tanpa_login(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        barang = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = Client().get('/detail/' + str(barang.id) + '/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Harap untuk melakukan",html_kembalian)
        self.assertIn("login",html_kembalian)
        self.assertEqual(response.status_code, 200)
    
    #templates
    def test_template_detail_page_is_using_template_detail(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        barang = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = Client().get('/detail/' + str(barang.id) + '/')
        self.assertTemplateUsed(response, 'detail/detailbarang.html')

    #Models
    def test_model_ulasan_create(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        ulasan_baru = Ulasan.objects.create(
            ulasan = 'wowwww',
            user = new_account,
            id_barang = 1,
        )
        count_all_ulasan = Ulasan.objects.all().count()
        self.assertEqual(count_all_ulasan, 1)

    def test_model_ulasan_str(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        ulasan_baru = Ulasan.objects.create(
            ulasan = 'keren',
            user = new_account,
            id_barang = 1,
        )
        ulasan_str = ulasan_baru.__str__()
        self.assertEqual(ulasan_str, 'keren')

    #views
    def test_view_detail_page_is_using_function_detail(self):
        found = resolve('/detail/1/')
        self.assertEqual(found.func, detailbarang)

    def test_view_simpanUlasan_page_is_using_function_simpanUlasan(self):
        found = resolve('/detail/simpanUlasan/')
        self.assertEqual(found.func, simpanUlasan)
    
    def test_view_detailbarang_can_save_ulasan_from_POST_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = c.post(
            '/detail/' + str(barang_baru.id) + '/',
            data = {
                'ulasan': 'emejing',
                'id_barang': barang_baru.id,
                'user': new_account,
            }
        )
        #count_all_ulasan = Ulasan.objects.all().count()
        #self.assertEqual(count_all_ulasan, 1)

        self.assertEqual(response.status_code, 200)

        response = c.get('/detail/' + str(barang_baru.id) + '/')
        html_response = response.content.decode('utf8')
        self.assertIn('Ulasan', html_response)

    

    