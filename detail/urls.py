from django.urls import path

from .views import detailbarang,simpanUlasan

app_name = 'detail'

urlpatterns = [
    path('<int:id_barang>/', detailbarang, name = 'detail'),
    path('simpanUlasan/',simpanUlasan,name='simpanUlasan'),
]