from django.shortcuts import render
from toko.models import Barang, Notifikasi
from django.contrib.auth.models import User
from .models import Ulasan
from .forms import UlasanForm
from django.core import serializers
from django.http import HttpResponse
import json
# Create your views here.

def detailbarang(request,id_barang):
    barang = Barang.objects.get(id=id_barang)
    ulasan = Ulasan.objects.filter(id_barang=id_barang)
    ulasanForm = UlasanForm(request.POST or None)
    if (request.method == "POST"):
        ulasanForm = UlasanForm(request.POST)
        if (ulasanForm.is_valid()):
            ulasanForm.save()
    context = {
        'barang' : barang,
        'ulasanForm' : ulasanForm,
        'ulasan' : ulasan,
    }
    return render(request, 'detail/detailbarang.html', context)

def simpanUlasan(request):
    response={}
    nama_input = ""
    if (request.method =='POST'):
            nama_input = request.POST.get('user', False)
            id_barang_input = request.POST.get('id_barang_input', False)
            ulasan_input = request.POST.get('input_ulasan', False)
            print (id_barang_input)
            if ( ulasan_input != ""):
                Ulasan.objects.create(
                    user = request.user,
                    id_barang = id_barang_input,
                    ulasan = ulasan_input
                ).save() 
    response['message'] = nama_input
    return HttpResponse(json.dumps(response), content_type="application/json")