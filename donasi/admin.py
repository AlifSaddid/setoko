from django.contrib import admin
from .models import Donasi, Donatur

# Register your models here.
admin.site.register(Donasi)
admin.site.register(Donatur)