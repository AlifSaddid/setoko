from django.forms import ModelForm
from django import forms
from .models import Donasi
# from django.contrib.auth.models import User

# tokos = [(i,i) for i in User.objects.all()]

class DonasiForm(ModelForm):
    class Meta:
        model = Donasi
        fields = '__all__'
        widgets = {
            # 'tokotujuan': forms.Select(choices=tokos, attrs={
            #     'placeholder': "Masukkan toko tujuan donasi",
            # }),
            'jumlahdonasi' : forms.NumberInput(attrs={
                'placeholder' : "Masukkan jumlah donasi yang diinginkan",
                'class' : "form-control"
            })
        }
        error_messages = {
            'tokotujuan': {
                'required': "Tujuan tidak boleh kosong",
            },
            'jumlahdonasi': {
                'required': "Jumlah donasi tidak boleh kosong",
                'invalid': "Masukkan jumlah yang valid"
            }
        }
        help_texts ={
            'jumlahdonasi' : 'Jumlah donasi minimal adalah 1000',
        }