from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError 

# Create your models here.
def validate_donasi(value):
    if (value >= 1000):
        return value
    else :
        raise ValidationError("Jumlah donasi harus lebih besar atau sama dengan 1000")

class Donasi(models.Model):
    pengirim = models.CharField(max_length = 100, default = "setokoadmin")
    tokotujuan = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
    jumlahdonasi = models.IntegerField('Jumlah Donasi ', validators=[validate_donasi])

    def __str__(self):
        return '{} {}'.format(self.tokotujuan, self.jumlahdonasi)

class Donatur(models.Model):
    namaDonatur = models.CharField(max_length=100)
    jumlahDonasi = models.IntegerField()

    def __str__(self):
        return '{} {}'.format(self.namaDonatur, self.jumlahDonasi)    