$(document).ready(function () {
    $(function () {
        $("#id_jumlahdonasi").on("input", function () {
            var nilai = $("#id_jumlahdonasi").val();
            // console.log(nilai);
            if (nilai >= 1000) {
                $(".btn-warning").removeAttr("disabled");
                $(".donasihelp").hide();
            } else {
                $(".btn-warning").attr("disabled", "disabled");
                $(".donasihelp").show();
            }
        });

        $("form").submit(function () {
            // catch the form's submit event
            console.log("asdasasd");
            $.ajax({
                // create an AJAX call...
                data: $(this).serialize(), // get the form data
                type: $(this).attr("method"), // GET or POST
                url: $(this).attr("action"), // the file to call
                success: function (response) {
                    // on success..
                  $(".donasicontainer").html(`
                    <div class="container-fluid d-flex align-item-center" style="height:80%">
                        <div class="container d-flex flex-column justify-content-center align-items-center">
                            <div>
                                <h1 class="mb-2">Terima kasih telah berdonasi.</h1>
                                <p class = "text-right mb-0">"No-one has become poor by giving"</p>
                                <p class = "text-right">- Anne Frank </p>
                            </div>
                            <a class="btn btn-warning" href="/" role="button">Kembali ke Halaman Depan</a>
                        </div>
                    </div>
                  `);
                },
            });
            return false;
        });
    });
});
