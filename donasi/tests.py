from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.shortcuts import render, redirect, resolve_url
from .models import Donasi, Donatur
from django.contrib.auth.models import User
from toko.models import Notifikasi

# Create your tests here.


class DonasiTest(TestCase):
    # URL
    def test_url_ada(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 302)

    def test_url_ada_login(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/donasi/')
        self.assertEqual(response.status_code, 200)

    def test_url_terimakasih_ada(self):
        response = Client().get('/donasi/terimakasih/')
        self.assertEqual(response.status_code, 302)

    def test_url_terimakasih_login(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/donasi/terimakasih/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_total_donasi(self):
        response = Client().get('/donasi/totaldonasi/')
        self.assertEqual(response.status_code, 200)

    # Model
    def test_tambah_donasi(self):
        new_account = User.objects.create_user('testing', '', 'test')
        new_account.save()
        donasi = Donasi.objects.create(
            tokotujuan=new_account, jumlahdonasi=1000)
        self.assertEqual(Donasi.objects.all().count(), 1)

    def test_tambah_donatur(self):
        donatur = Donatur.objects.create(namaDonatur="tes", jumlahDonasi=10000)
        donatur.save()
        self.assertEqual(Donatur.objects.all().count(), 1)
        self.assertEqual(str(donatur), 'tes 10000')

    def test_print_donasi(self):
        new_account = User.objects.create_user('testing', '', 'test')
        new_account.save()
        donasi = Donasi.objects.create(
            tokotujuan=new_account, jumlahdonasi=1000)
        self.assertEqual(str(donasi), 'testing 1000')

    # views
    def test_save_donasi_post(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.post(
            '/donasi/',
            data={
                'pengirim': 'orang baik',
                'tokotujuan': new_account.id,
                'jumlahdonasi': 1000
            }
        )
        count = Donasi.objects.all().count()
        self.assertEqual(count, 1)
        count1 = User.objects.all().count()
        self.assertEqual(count1, 1)
        self.assertEqual(response.status_code, 302)

    def test_donasi_kurang_dari_1000_post(self):
        try:
            new_account = User.objects.create_user('testing', '', 'test123')
            new_account.set_password('test123')
            new_account.save()
            c = Client()
            login = c.login(username=new_account.username, password='test123')
            response = c.post(
                '/donasi/',
                data={
                    'tokotujuan': new_account.id,
                    'jumlahdonasi': 100
                }
            )
        finally:
            count = Donasi.objects.all().count()
            self.assertEqual(count, 0)

    def test_save_donasi_notif(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.post(
            '/donasi/',
            data={
                'pengirim': 'Saya',
                'tokotujuan': new_account.id,
                'jumlahdonasi': 1000
            }
        )
        response2 = c.get('/donasi/')
        count = Notifikasi.objects.all().count()
        self.assertEqual(count, 1)

    def test_terimakasih_tanpa_login(self):
        response = Client().get('/donasi/terimakasih/')
        self.assertEqual(response.status_code, 302)

    def test_api(self):
        response = Client().get('/donasi/api/')
        self.assertEqual(response.status_code, 200)

    def test_api_2(self):
        response = Client().get('/donasi/api/something')
        self.assertEqual(response.status_code, 200)

    def test_terimakasih_login(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/donasi/terimakasih/')
        self.assertEqual(response.status_code, 200)

    def test_terimakasih_login_response(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/donasi/terimakasih/')
        html_response = response.content.decode('utf8')
        self.assertIn("Kembali ke Halaman Depan", html_response)

    # Template
    def test_donasi_after_login_template(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/donasi/')
        self.assertTemplateUsed(response, 'donasi/index.html')

    def test_terimakasih_after_login_template(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/donasi/terimakasih/')
        self.assertTemplateUsed(response, 'donasi/terimakasih.html')
