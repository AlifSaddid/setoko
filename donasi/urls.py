from django.urls import path
from . import views

app_name = 'donasi'

urlpatterns = [
    path('', views.donasi, name = 'donasi'),
    path('terimakasih/', views.terimakasih, name = 'terimakasih'),
    path('totaldonasi/', views.totaldonasi, name = 'totaldonasi'),
    path('api/', views.donasi_api2, name = 'api2'),
    path('api/<str:nama>', views.donasi_api, name = 'api'),
]