from django.shortcuts import render,redirect, resolve_url
from .forms import DonasiForm
from django.urls import reverse
from django.contrib.auth.models import User 
from django.contrib.auth import get_user_model
from toko.models import Notifikasi
from django.core import serializers
from .models import Donasi, Donatur
from django.contrib import messages
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.

def donasi(request):
    User = get_user_model()
    users = User.objects.all()
    usernow = request.user.username
    donasierror = False
    if (request.user.is_anonymous):
        return redirect('authuser:login')

    formDonasi= DonasiForm()
    if request.method == 'POST':
        form = DonasiForm(request.POST or None)
        namaDonatur = request.POST.get('pengirim', 'Hamba Allah')
        jumlahDonasi = request.POST.get('jumlahdonasi')

        try:
            # print("sukgasih")
            donaturObj = Donatur.objects.get(namaDonatur = namaDonatur)
            donaturObj.jumlahDonasi += int(jumlahDonasi)
            # print("hah")
            donaturObj.save()
            # print("Masuk1")
        except ObjectDoesNotExist:
            newDonatur = Donatur(namaDonatur=namaDonatur, jumlahDonasi=jumlahDonasi)
            newDonatur.save()
            # print("Masuk2")

        if form.is_valid():
            form.save()
            pengirim = request.POST['pengirim']
            # print(pengirim)
            toko_tujuan = User.objects.get(id = request.POST['tokotujuan'])
            jumlah_donasi = request.POST['jumlahdonasi']
            isi_notifikasi = 'Pengguna ' + request.user.username + ' memberikan anda donasi sejumlah Rp ' + jumlah_donasi
            notif_baru = Notifikasi.objects.create(penjual = toko_tujuan, isi = isi_notifikasi)
            notif_baru.save()
            return redirect('/donasi/terimakasih')
        else:
            donasierror = True
            context = {
                'formdonasi' : formDonasi,
                'users' : users,
                'usernow' : usernow,
                'donasierror' : donasierror
            }
            return render(request, 'donasi/index.html', context)

    context = {
        'formdonasi' : formDonasi,
        'users' : users,
        'usernow' : usernow,
        'donasierror' : donasi
    }

    return render(request, 'donasi/index.html', context)

def terimakasih(request):
    if (request.user.is_anonymous):
        return redirect('authuser:login')
    context = {}
    return render (request,'donasi/terimakasih.html')
    

def totaldonasi(request):
    total = 0
    allDonasi = Donasi.objects.all()
    userandtotal = {}
    for donasi in allDonasi:
        total += donasi.jumlahdonasi
        if (not donasi.pengirim in userandtotal):
            userandtotal[donasi.pengirim] = donasi.jumlahdonasi
        else :
            userandtotal[donasi.pengirim] += donasi.jumlahdonasi
    # print(userandtotal)
    sorteduserandtotal = sorted(userandtotal.items(), key=lambda userandtotal: userandtotal[1], reverse=True) 
    # print(sorteduserandtotal)
    context={
        'total' : total,
        'userandtotal' : sorteduserandtotal,
    }
    return render (request,'donasi/totaldonasi.html',context)

def donasi_api(request,nama):
    json_data = serializers.serialize('json', Donatur.objects.filter(namaDonatur__icontains=nama).order_by('jumlahDonasi','namaDonatur').reverse())    
    # print(json_data)
    return JsonResponse(json_data, safe=False)

def donasi_api2(request):
    # print(type(Donatur.objects.all()))
    json_data = serializers.serialize('json', Donatur.objects.order_by('jumlahDonasi','namaDonatur').reverse())  
    # print(json_data)
    return JsonResponse(json_data, safe=False)