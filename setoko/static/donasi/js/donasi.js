$(document).ready(function () {
    $(function () {
      $("#id_jumlahdonasi").on('input', function () {
        var nilai = $("#id_jumlahdonasi").val();
        // console.log(nilai);
        if (nilai >= 1000){
            $(".btn-warning").removeAttr('disabled');
            $(".donasihelp").hide();
        } else {
            $(".btn-warning").attr('disabled', 'disabled');
            $(".donasihelp").show();
        }
      });
    });
  });
  