$(document).ready(function () {
    
    function firstAjax(){
        $.ajax({
            url: "/donasi/api/",
            dataType : 'json',
            success: function (result) {
                var donaturs = JSON.parse(result);
                // console.log(donaturs);
                donaturs.forEach(cetakDonatur);
                function cetakDonatur(donatur,i){
                    let nama = donatur.fields.namaDonatur;
                    let jumlah = donatur.fields.jumlahDonasi;
                    $("#donatur").append (`
                    <tr>
                    <td>`+ (i+1) + `</td>
                    <td>`+nama+`</td>
                    <td>`+jumlah+`</td>
                    </tr>
                    `);
                }
            },
        });
    }

    firstAjax();

    $(".searchnama").keyup(function () {
        var keyword = $(".searchnama").val();
        // console.log(keyword);
        $.ajax({
          url: "/donasi/api/" + keyword,
          dataType : 'json',
          success: function (result) {
            var donaturs = JSON.parse(result);
            $("tbody").empty();
            // console.log(donaturs);
            donaturs.forEach(cetakDonatur);
            function cetakDonatur(donatur,i){
                let nama = donatur.fields.namaDonatur;
                let jumlah = donatur.fields.jumlahDonasi;
                $("#donatur").append (`
                <tr>
                <td>`+ (i+1) + `</td>
                <td>`+nama+`</td>
                <td>`+jumlah+`</td>
                </tr>
                `);
            };
          },
        });
      });

    $(".searchnama").focus(function(){
        $(this).css("border-color", "#FF6633");
    })

    $(".searchnama").blur(function(){
        $(this).css("border-color", "");
    })


    var now = 1;
    $("button").click(function(){
        $(".searchnama").val('');
        $(".searchnama").toggle();
        if (now == 1){
            console.log(this)
            $(this).text("Tampilkan semua donatur");
            $("tbody").empty();
            now = 2;
        } else {
            $(this).text("Cari Berdasarkan Nama Donatur");
            $("tbody").empty();
            firstAjax();
            now = 1;
        }
    });
});
