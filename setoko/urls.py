from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('authuser.urls', namespace = 'authuser')),
    path('', include('toko.urls', namespace = 'toko')),
    path('contactus/', include('contactus.urls', namespace = 'contactus')),
    path('donasi/', include('donasi.urls', namespace = 'donasi')),
    path('detail/', include('detail.urls', namespace = 'detail')),
    path('checkout/', include('checkout.urls', namespace = 'checkout')),
]

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)