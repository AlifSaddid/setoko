from django.contrib import admin
from .models import Barang, Notifikasi, Cart

# Register your models here.
admin.site.register(Barang)
admin.site.register(Notifikasi)
admin.site.register(Cart)