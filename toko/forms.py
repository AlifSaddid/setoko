from django import forms
from .models import Barang

class BarangForm(forms.ModelForm):
    class Meta:
        model = Barang
        fields = [
            'nama',
            'deskripsi',
            'harga',
            'gambar',
            'penjual',
            'kategori'
        ]

        widgets = {
            'nama': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan Nama Barang'
                }
            ),
            'deskripsi': forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan Deskripsi Barang'
                }
            ),
            'harga': forms.NumberInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan Harga Barang'
                }
            ),
            'gambar': forms.FileInput(
                attrs = {
                    'class': 'form-control-file dropzone',
                }
            ),
            'kategori': forms.Select(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan Kategori Barang'
                }
            ),
        }