# Generated by Django 3.1.2 on 2020-11-09 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toko', '0006_notifikasi_tanggal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notifikasi',
            name='tanggal',
            field=models.DateField(auto_now_add=True),
        ),
    ]
