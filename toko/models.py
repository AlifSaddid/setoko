from django.db import models
from django.conf import settings

# Create your models here.
class Barang(models.Model):
    nama        = models.CharField(max_length = 300)
    deskripsi   = models.CharField(max_length = 1500)
    harga       = models.IntegerField()
    gambar      = models.ImageField(upload_to = 'images/')
    penjual     = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete = models.CASCADE,
    )
    kategori    = models.CharField(
        max_length = 100,
        choices = [
            ('Makanan', 'Makanan'),
            ('Minuman', 'Minuman'),
            ('Perabotan', 'Perabotan'),
            ('Mainan', 'Mainan'),
            ('Kerajinan', 'Kerajinan'),
            ('Alat Tulis', 'Alat Tulis'),
            ('Pakaian', 'Pakaian'),
            ('Handphone & Aksesoris', 'Handphone & Aksesoris'),
            ('Kesehatan', 'Kesehatan'),
            ('Pakaian Pria', 'Pakaian Pria'),
            ('Elektronik', 'Elektronik'),
            ('Hobi & Koleksi', 'Hobi & Koleksi'),
            ('Pakaian Wanita', 'Pakaian Wanita'),
            ('Tas Wanita', 'Tas Wanita'),
            ('Perawatan & Kecantikan', 'Perawatan & Kecantikan'),
            ('Perlengkapan Rumah', 'Perlengkapan Rumah'),
            ('Komputer & Aksesoris', 'Komputer & Aksesoris'),
            ('Buku & Alat Tulis', 'Buku & Alat Tulis'),
            ('Lain-Lain', 'Lain-Lain')
        ]    
    )

    def __str__(self):
        return '{}. {}'.format(self.id, self.nama)

class Notifikasi(models.Model):
    penjual = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete = models.CASCADE,
    )
    isi = models.CharField(max_length = 1000)
    tanggal = models.DateTimeField(auto_now_add = True)
    readed = models.BooleanField(default = False)
    
    class Meta:
        ordering = ['-tanggal']

class Cart(models.Model):
    penjual = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete = models.CASCADE
    )
    id_barang = models.IntegerField()
    jumlah_barang = models.IntegerField()