from django.test import TestCase, Client
from django.urls import resolve
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile
from io import BytesIO, StringIO

from django.contrib.auth.models import User 
from .models import Barang, Notifikasi, Cart
from .views import index, toko, tambah, search_keyword, search_category, edit, hapus, toko_user

# Create your tests here.
class TokoTest(TestCase):
    
    '''
        URL Testcases
    '''
    def test_url_is_exist_homepage(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_toko(self):
        response = Client().get('/toko/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_toko_tambah(self):
        response = Client().get('/toko/tambah/')
        self.assertEqual(response.status_code, 302)

    def test_url_is_exist_toko_category(self):
        response = Client().get('/toko/search/category/makanan/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_toko_search(self):
        response = Client().get('/toko/search/keyword/odading/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_toko_cart(self):
        response = Client().get('/toko/user/cart/')
        self.assertEqual(response.status_code, 302)

    def test_url_is_exist_toko_user(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        response = Client().get('/toko/user/1/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_exist_toko_edit_get_failed(self):
        response = Client().get('/toko/edit/0/')
        self.assertEqual(response.status_code, 302)

    def test_url_is_exist_toko_hapus(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = Client().get('/toko/hapus/' + str(barang_baru.id) + '/')
        self.assertEqual(response.status_code, 302)

    '''
        Model Testcases
    '''
    def test_model_barang_create(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )

        count_all_barang = Barang.objects.all().count()
        self.assertEqual(count_all_barang, 1)

    def test_model_barang_delete(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )

        barang_baru.delete()

        count_all_barang = Barang.objects.all().count()
        self.assertEqual(count_all_barang, 0)
    
    def test_model_barang_str(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )

        barang_str = barang_baru.__str__()
        self.assertEqual(barang_str, '1. Barang')

    def test_model_notifikasi_create(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        notif_baru = Notifikasi.objects.create(
            penjual = new_account,
            isi = "Penjual lain memberi anda donasi sebesar 100000"
        )
        count_all_barang = Notifikasi.objects.all().count()
        self.assertEqual(count_all_barang, 1)

    def test_model_notifikasi_delete(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        notif_baru = Notifikasi.objects.create(
            penjual = new_account,
            isi = "Penjual lain memberi anda donasi sebesar 100000"
        )
        notif_baru.delete()
        count_all_barang = Notifikasi.objects.all().count()
        self.assertEqual(count_all_barang, 0)

    
    '''
        Views Testcases
    '''
    def test_view_landing_page_is_using_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_view_toko_page_is_using_function_index(self):
        found = resolve('/toko/')
        self.assertEqual(found.func, toko)

    def test_view_search_category_page_is_using_function_search_category(self):
        found = resolve('/toko/search/category/makanan/')
        self.assertEqual(found.func, search_category)
    
    def test_view_search_keyword_is_using_function_search_keyword(self):
        found = resolve('/toko/search/keyword/odading/')
        self.assertEqual(found.func, search_keyword)

    def test_view_tambah_page_is_using_function_tambah(self):
        found = resolve('/toko/tambah/')
        self.assertEqual(found.func, tambah)

    def test_view_edit_page_is_using_function_edit(self):
        found = resolve('/toko/edit/1/')
        self.assertEqual(found.func, edit)

    def test_view_delete_page_is_using_function_hapus(self):
        found = resolve('/toko/hapus/1/')
        self.assertEqual(found.func, hapus)

    def test_view_mytoko_page_is_using_function_toko_user(self):
        found = resolve('/toko/user/1/')
        self.assertEqual(found.func, toko_user)
    
    def test_view_tambah_can_save_barang_from_POST_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'Barang',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )

        count_all_barang = Barang.objects.all().count()
        self.assertEqual(count_all_barang, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/toko/user/' + str(new_account.id) + '/')

        response = c.get('/toko/')
        html_response = response.content.decode('utf8')
        self.assertIn('Barang', html_response)

    def test_view_hapus_confirmation(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'nama',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )
        
        response = c.get(
            '/toko/hapus/' + '1' + '/'
        )

        html_response = response.content.decode('utf8')
        self.assertIn('Apakah Anda Yakin Ingin Menghapus Barang nama?', html_response)

    def test_view_hapus_can_delete_barang_from_POST_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'nama',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )

        response = c.post(
            '/toko/hapus/' + '1' + '/'
        )

        count_all_barang = Barang.objects.all().count()
        self.assertEqual(count_all_barang, 0)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/toko/user/' + str(new_account.id) + '/')

    def test_view_edit_can_edit_barang_from_POST_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )

        response = c.get(
            '/toko/edit/' + str(barang_baru.id) + '/'
        )
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn('Edit Barang ' + barang_baru.nama, html_response)
    
    def test_view_edit_cannot_edit_barang_from_anonymous_user(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = Client().get('/toko/edit/1/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/toko/')


    def test_view_edit_can_save_edit_barang_from_POST_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )

        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/edit/' + str(barang_baru.id) + '/',
            data = {
                'nama': 'Nama Baru',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': barang_baru.penjual.id,
                'kategori': 'Makanan', 
            }
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/toko/user/' + str(new_account.id) + '/')
        response = c.get('/toko/user/1/')
        count_all_barang = Barang.objects.all().count()
        self.assertEqual(count_all_barang, 1)
        html_response = response.content.decode('utf8')
        self.assertIn('Nama Baru', html_response)

    def test_view_edit_cannot_edit_barang_from_different_seller(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        another_account = User.objects.create_user('another', '', 'password_for_another')
        another_account.set_password('password_for_another')
        another_account.save()
        c = Client()
        login = c.login(username=another_account.username, password='password_for_another')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = c.get('/toko/edit/1/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/toko/')
    
    def test_view_search_keyword_can_search_without_keyword(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'Odading Mang Oleh',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )

        response = c.post(
            '/toko/search/keyword/'
        )
        self.assertEqual(response.status_code, 302)
        response = c.get('/toko/search/keyword/')
        html_response = response.content.decode('utf8')
        self.assertIn('Odading Mang Oleh', html_response)

        response = c.post(
            '/toko/search/keyword/x/',
            data = {
                'keyword': ''
            }
        )
        self.assertEqual(response.status_code, 302)

    def test_view_search_keyword_can_search(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'Odading Mang Oleh',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )

        response = c.post(
            '/toko/search/keyword/odading/',
            data = {
                'keyword': 'odading'
            }
        )
        self.assertEqual(response.status_code, 302)
        response = c.get('/toko/search/keyword/odading/')
        html_response = response.content.decode('utf8')
        self.assertIn('Odading Mang Oleh', html_response)
    
    def test_view_search_category_can_search(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'Odading Mang Oleh',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )

        response = c.get('/toko/search/category/makanan/')
        html_response = response.content.decode('utf8')
        self.assertIn('Odading Mang Oleh', html_response)
    
    def test_view_hapus_page_cannot_delete_from_anonymous_user(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = Client().get('/toko/hapus/1/')
        self.assertEqual(response.status_code, 302)
    
    def test_view_cart_can_access_from_authenticated_user(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        response = c.get('/toko/user/cart/')
        self.assertEqual(response.status_code, 200)

    def test_view_cart_can_access_from_post_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        im = Image.new(mode='RGB', size=(200, 200)) # create a new image using PIL
        im_io = BytesIO() # a BytesIO object for saving image
        im.save(im_io, 'JPEG') # save the image to im_io
        im_io.seek(0) # seek to the beginning
        image = InMemoryUploadedFile(im_io, None, 'random-name.jpg', 'image/jpeg', len(im_io.getvalue()), None)
        response = c.post(
            '/toko/tambah/',
            data = {
                'nama': 'Odading Mang Oleh',
                'harga': 5000,
                'deskripsi': 'Test Barang',
                'gambar': image,
                'penjual': new_account.id,
                'kategori': 'Makanan', 
            }
        )

        response = c.post(
            '/toko/user/cart/',
            data = {
                'id_barang': 1,
                'menu_1': 10
            }
        )
        count_all_barang = Cart.objects.all().count()
        self.assertEqual(count_all_barang, 1)


    '''
        Templates Testcases
    '''
    def test_template_landing_page_is_using_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'toko/index.html')

    def test_template_search_category_page_is_using_template_toko(self):
        response = Client().get('/toko/search/category/makanan/')
        self.assertTemplateUsed(response, 'toko/toko.html')

    def test_template_search_keyword_page_is_using_template_toko(self):
        response = Client().get('/toko/search/keyword/odading/')
        self.assertTemplateUsed(response, 'toko/toko.html')

    def test_template_tambah_page_is_using_template_Barangform(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        response = c.get('/toko/tambah/')
        self.assertTemplateUsed(response, 'toko/Barangform.html')

    def test_template_mytoko_page_is_using_template_toko(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.save()
        response = Client().get('/toko/user/1/')
        self.assertTemplateUsed(response, 'toko/toko.html')

    def test_template_hapus_page_is_using_template_hapus_confirmation(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = c.get('/toko/hapus/1/')
        self.assertTemplateUsed(response, 'toko/hapus_confirmation.html')

    def test_template_edit_page_is_using_template_Barangform(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='password_for_dummy')
        barang_baru = Barang.objects.create(
            nama = 'Barang',
            harga = 5000,
            deskripsi = 'Test Barang',
            gambar = 'null',
            penjual = new_account,
            kategori = 'Makanan', 
        )
        response = c.get('/toko/edit/1/')
        self.assertTemplateUsed(response, 'toko/Barangform.html')