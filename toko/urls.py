from django.urls import path

from .views import index, toko, tambah, search_keyword, search_keyword_all, search_category, edit, hapus, toko_user, clean_notif, get_notif, cart, add_to_cart

urlpatterns = [
    path('toko/', toko, name = 'toko'),
    path('toko/tambah/', tambah, name = 'tambah'),
    path('toko/search/keyword/<str:keyword>/', search_keyword, name='search_keyword'),
    path('toko/search/keyword/', search_keyword_all, name='search_keyword_all'),
    path('toko/search/category/<str:category>/', search_category, name='search_category'),
    path('toko/edit/<int:id_barang>/', edit, name='edit'),
    path('toko/hapus/<int:id_barang>/', hapus, name='hapus'),
    path('toko/user/<int:id_user>/', toko_user, name='toko_user'),
    path('toko/user/cart/', cart, name='cart'),
    path('toko/api/notif/clean/', clean_notif, name='clean_notif'),
    path('toko/api/notif/get/', get_notif, name='get_notif'),
    path('toko/api/cart/add/', add_to_cart, name='add_to_cart'),
    path('', index, name = 'index'),
]

app_name = 'toko'