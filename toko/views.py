from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Barang, Notifikasi, Cart
from .forms import BarangForm
from django.contrib.auth.models import User 
from django.core import serializers
import json

# Landing Page
def index(request):

    items = Barang.objects.all()
    context = {
        'Items': items,
    }

    return render(request, 'toko/index.html', context)

# All Barang
def toko(request):
    items = Barang.objects.all()
    context = {
        'Items': items,
        'queryResult': 'semua barang'
    }

    return render(request, 'toko/toko.html', context)

# Search by category
def search_category(request, category):

    items = Barang.objects.all()
    items = [i for i in items if (category.lower() in i.kategori.lower())]
    context = {
        'Items': items,
        'queryResult': 'barang dengan kategori ' + category
    }

    return render(request, 'toko/toko.html', context)

# Search by keyword
def search_keyword(request, keyword):

    if (request.method == "POST" and request.POST['keyword'] == ''):
        return redirect('toko:search_keyword_all')

    if (request.method == "POST"):
        return redirect('toko:search_keyword', keyword = request.POST['keyword'])

    items = Barang.objects.all()
    items = [i for i in Barang.objects.all() if (keyword in "".join(i.nama.split()).lower())]
    context = {
        'Items': items,
        'queryResult': 'barang dengan kata kunci ' + keyword
    }

    return render(request, 'toko/toko.html', context)

def search_keyword_all(request):

    if (request.method == "POST"):
        return redirect('toko:search_keyword_all')

    items = Barang.objects.all()
    context = {
        'Items': items,
        'queryResult': 'Menampilkan semua barang '
    }

    return render(request, 'toko/toko.html', context)

# Tambah Barang
def tambah(request):

    if (request.user.is_anonymous):
        return redirect('authuser:login')

    form = BarangForm(request.POST or None)

    context = {
        'Heading': 'Tambah Barang',
        'Form': form,
        'imageurl': None,
        'successButtonText': 'Jual'
    }

    if (request.method == "POST"):
        form = BarangForm(request.POST, request.FILES)
        if (form.is_valid()):
            form.save()
        return redirect('toko:toko_user', id_user = request.user.id )

    return render(request, 'toko/Barangform.html', context)

# Edit Barang
def edit(request, id_barang):
    barang = ""
    try:
        barang = Barang.objects.get(id = id_barang)
    except:
        return redirect('toko:toko')
    if (request.method == "GET"):
        if (request.user.is_anonymous):
            return redirect('toko:toko')
        elif (request.user.id != barang.penjual.id):
            return redirect('toko:toko')
    
    if (request.method == "POST"):
        form = BarangForm(request.POST, request.FILES, instance = barang)
        if (form.is_valid()):
            form.save()
        return redirect('toko:toko_user', id_user = request.user.id )
    
    form = BarangForm(initial = {
        'nama': barang.nama,
        'deskripsi': barang.deskripsi,
        'harga': barang.harga,
        'gambar': barang.gambar,
        'penjual': barang.penjual,
        'kategory': barang.kategori
    }, instance = barang)

    context = {
        'Barang': barang,
        'Form': form,
        'Heading': 'Edit Barang ' + barang.nama,
        'imageurl': barang.gambar.url,
        'successButtonText': 'Simpan'
    }

    return render(request, 'toko/Barangform.html', context)

# Hapus Barang
def hapus(request, id_barang):

    if (request.user.is_anonymous):
        return redirect('authuser:login')

    barang = Barang.objects.get(id = id_barang)
    context = {
        'barang': barang
    }
    if (request.method == "POST"):
        barang.delete()
        return redirect('toko:toko_user', id_user = request.user.id)

    return render(request, 'toko/hapus_confirmation.html', context)

# Toko saya
def toko_user(request, id_user):
    items = Barang.objects.filter(penjual = User.objects.get(id = id_user))
    context = {
        'Items': items,
        'queryResult': 'barang dari penjual ' + User.objects.get(id = id_user).username
    }

    return render(request, 'toko/toko.html', context)

def cart(request):
    if (request.user.is_anonymous):
        return redirect('toko:toko')
    
    if (request.method == "POST"):
        cart = Cart.objects.create(penjual = request.user, id_barang = request.POST['id_barang'], jumlah_barang = request.POST['menu_1'])
        cart.save()
        return redirect('toko:cart')

    items = Cart.objects.filter(penjual = User.objects.get(id = request.user.id))
    items = [(Barang.objects.get(id = item.id_barang), item.jumlah_barang, item.id) for item in items]
    context = {
        'items': items
    }
    return render(request, 'toko/cart.html', context)

# Api Clean Notif
def clean_notif(request):
    allNotif = Notifikasi.objects.filter(penjual = request.user)
    for notif in allNotif:
        notif.readed = True
        notif.save()
    data = [{'success': True}]
    data = json.dumps(data)
    return HttpResponse(content = data, content_type="application/json")

# Api get notif
def get_notif(request):
    notif_json = serializers.serialize("json", Notifikasi.objects.filter(penjual = request.user))
    notif_json = json.dumps(notif_json)
    return HttpResponse(notif_json, content_type="application/json")

#Tambah barang
def add_to_cart(request):
    cart_baru = Cart( penjual=request.user, id_barang=int(request.POST.get("id_barang",False)), jumlah_barang=int(request.POST.get("jumlah",False)))
    cart_baru.save()
    data = [{'success': True}]
    data = json.dumps(data)
    return HttpResponse(content = data, content_type="application/json")

